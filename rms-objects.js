function Asset(id, name, energyType, loc, status, lf, ap) {
    this.id = id;
    this.name = name;
    this.energyType = energyType;
    this.loc = loc || null;
    this.status = status || null;
    this.lf = lf || null;
    this.ap = ap || null;
}

Asset.prototype.toString = function () {
    return this.id;
}

function Park(id, name, energyType, assets, loc, radius, lf, ap, online, offline, nocomm, weather) {
    this.id = id;
    this.name = name;
    this.energyType = energyType;
    this.assets = assets;
    this.loc = loc;
    this.radius = radius;
    this.lf = lf;
    this.ap = ap;
    this.online = online;
    this.offline = offline;
    this.nocomm = nocomm;
    this.weather = weather;
    this.type = "park";
}

Park.prototype.toString = function () {
    return this.id;
}

function Region(id, name, acronym, loc, radius, lf, ap, online, offline, nocomm, weather) {
    this.id = id;
    this.name = name;
    this.acronym = acronym;
    this.loc = loc;
    this.radius = radius;
    this.lf = lf;
    this.ap = ap;
    this.online = online;
    this.offline = offline;
    this.nocomm = nocomm;
    this.weather = weather;
    this.type = "region";
}

Region.prototype.toString = function () {
    return this.id;
}

function Country(id, name, acronym, loc, radius, lf, ap, online, offline, nocomm, weather) {
    this.id = id;
    this.name = name;
    this.acronym = acronym;
    this.loc = loc;
    this.radius = radius;
    this.lf = lf;
    this.ap = ap;
    this.online = online;
    this.offline = offline;
    this.nocomm = nocomm;
    this.weather = weather;
    this.type = "country";
}

Country.prototype.toString = function () {
    return this.id;
}

function Owner(id, name, loc, radius, lf, ap) {
    this.id = id;
    this.name = name;
    this.loc = loc;
    this.radius = radius;
    this.lf = lf;
    this.ap = ap;
    this.type = "owner";
}

Owner.prototype.toString = function () {
    return this.id;
}

function KPIInfo(id, name, description) {
    this.id = id;
    this.name = name;
    this.descpription = description;
}

function KPIValue(kpi_info, unit, value) {
    this.kpi = kpi;
    this.unit = unit;
    this.value = value;
}

function Location(latitude, longitude, altitude) {
    this.lat = latitude;
    this.lon = longitude;
    this.alt = altitude || 0;
}

//Location.prototype.calcDistance = function (loc) {
//    return this.latitude + this.longitude + loc.latitude + loc.longitude;
//};
//Location.prototype.calcDistance3D = function (loc) {
//    return calcDistance(loc) + this.altitude + loc.altitude;
//};