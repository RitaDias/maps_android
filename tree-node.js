
function Tree() {
    this.root = new Node('root');
    this.nodes_by_levels = [];
    this.nodes_by_levels_id = [];
    this.nodes_by_levels[this.root.level] = [this.root];
    this.nodes_by_levels_id[this.root.level] = [];
    this.nodes_by_levels_id[this.root.level]['root'] = this.root;
}

Tree.prototype.addChild = function (parent, child) {
    child.setParent(parent);
    if (child.level === this.nodes_by_levels.length) {
        this.nodes_by_levels.push([]);
        this.nodes_by_levels_id.push([]);
    }
    this.nodes_by_levels[child.level].push(child);
    this.nodes_by_levels_id[child.level][child.value.id] = child;
}

Tree.prototype.getNodesInLevel = function (level) {
    return this.nodes_by_levels[level];
}

Tree.prototype.findNodeInLevel = function (level, node_id) {
    return this.nodes_by_levels_id[level][node_id];
}

Tree.prototype.getTreeDepth = function () {
    return this.nodes_by_levels.length;
}

Tree.prototype.listNode = function (node) {

    var spaces = this.getStringSpacesByLevel(node, "-");
    console.log(spaces + node.value)

    for (var i = 0 ; i < node.children.length ; i++) {
        this.listNode(node.children[i]);
    }
}

Tree.prototype.getStringSpacesByLevel = function (node, space_string) {
    var spaces = "";

    for(var i = 0 ; i < node.level ; i++) {
        spaces += space_string;
    }
    return spaces;
}



function Node(value) {
    this.value = value;
    this.children = [];
    this.parent = null;
    this.level = 0;
}

Node.prototype.setParent = function (parent) {
    this.parent = parent;
    this.level = this.parent.level + 1;
    this.parent.children[parent.children.length] = this;
}