
var tree_nodes = new Tree();
var tree_root = tree_nodes.root;
var node_focused = null;

var ctxArray = [];
var ctxArrayRegions = [];
var firstDraw = true;

var firstTime = 1;
var firstTimeState = 1;
var map;
var southWest;
var northEast;
var bounds;

var canvasTiles = L.tileLayer.canvas();
var owner;
var jAssets = {};
var regions = {};
var countries = {};
var parksMap = {};
var polygons = {};
var ownerCircle;
var parkCircle;
var regionCircle;
var eolics;
var solars;
var hydrics;
var assets;
var eolicParksIcon = L.divIcon({
    className: 'park-icon-wpp marker-cluster-eolicPark'
});
var solarParksIcon = L.divIcon({
    className: 'park-icon-spp marker-cluster-solarPark'
});
var hydroParksIcon; L.divIcon({
    className: 'park-icon-hpp marker-cluster-hydroPark'
});
/*var eolicIcon = L.divIcon({
    className: 'park-icon-wtg marker-cluster-eolic'
});*/

var ownerCircleLayer;
var parkCirclesLayer;
var regionCirclesLayer;
var addedOwnerCircle = false;
var addedCountryCircle = false;
var addedParkCircles = false;
var addedRegionCircles = false;
var addedPolygons = false;
var parksExists = false;
var addedAssets = false;
var ownerPopup;
var regionsPopups = [];
var parksPopups = [];
var multiPolygon;
var polygonGroup;

var ownerGroup = [];
var regionGroup = [];
var parkGroup = [];


var markersClick = [];
var popupsClick = [];

var isReady = 0;

var setAssetsZoom;
var setParkRegionCircleMin;
var setParkRegionCircleMax;
var setCountryCircleMin;
var setCountryCircleMax;
var ownerCircleMin;
var ownerCircleMax;

function init() {
    // Hack to keep more than one popup open
    //L.Map = L.Map.extend({
    //    openPopup: function (popup) {
    //        //this.closePopup();  // just comment this
    //        this._popup = popup;

    //        return this.addLayer(popup).fire('popupopen', {
    //            popup: this._popup
    //        });
    //    }
    //});

    map = L.map('leafletMap', {
        touchZoom: true,
        dragging: true,
        zoomControl: false,
        closePopupOnClick: false
    }).setView([39.678, -8.229], 5);

    // Initialize the SVG layer 
    //map._initPathRoot();

    // SVG from the map object 
    //var svg = d3.select("#map").select("svg");

    // Map bounds to prevent user from get out 
    southWest = [85.05, -180.01];
    northEast = [-85.06, 180.05];

    bounds = L.latLngBounds(southWest, northEast);

    map.setMaxBounds(bounds);
    // Tile properties 
    L.tileLayer('https://api.tiles.mapbox.com/v4/bmpgp.010fd6a5/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYm1wZ3AiLCJhIjoiOWY4NGYwN2VjZDg0MGI1ZjdmMWI3ZjdlNGNmY2NmNmQifQ.FZ5cr4mO3iDKVkx9zz4Nkg', {
        attribution: 'RMS � Powered By <a href="http://www.cgi.com">CGI</a>',
        minZoom: 4,
        maxZoom: 18,
        id: 'bmpgp.010fd6a5',
        accessToken: 'pk.eyJ1IjoiYm1wZ3AiLCJhIjoiOWY4NGYwN2VjZDg0MGI1ZjdmMWI3ZjdlNGNmY2NmNmQifQ.FZ5cr4mO3iDKVkx9zz4Nkg'
    }).addTo(map);

    // Popup Handling
    map.on('popupopen', function (e) {

        if (e.popup.options.type === 'park') {

            // Assets' Icons
            $('.asset-icons').css("zIndex", "1");

            // Park
            var className = '.parks-icons-' + e.popup.options.id;
            $('.parks-icons').css("zIndex", "2");
            $('.park-popup').css("zIndex", "3");
            $(className).css("zIndex", "400");
        }
        else if (e.popup.options.type === 'asset') {

            // Parks' Icons
            $('.parks-icons').css("zIndex", "1");

            // Asset
            var className = '.asset-' + e.popup.options.parkId + '-' + e.popup.options.id;
            $('.asset-icons').css("zIndex", "1");
            $('.asset-popup').css("zIndex", "2");
            $(className).css("zIndex", "300");
        }
    });

    //PolygonsConstruction();

    /*assets = new L.MarkerClusterGroup({
        spiderfyOnMaxZoom: false,
        disableClusteringAtZoom: 4
    });*/

    //markerCircle._icon.style.display = 'none';

    //Zoom End function 
    // 1: setAssetsZoom, 2: setParkCircleZoom, 3: setParkIconStart, 4: setParkIconStop, 5: setRegionCircleStart, 6: setRegionCircleStop, 7: OwnerCircleStart, 8: OwnerCircleStop 12, 12, 4, 12, 7, 11, 4, 7
}

function OwnerConstruction(jOwner) {
    var ownerRadius = 300000; //jOwner.map.radius;
    if (ownerRadius < 100000) {
        ownerRadius = 300000;
    }

    owner = {
        id: jOwner.id,
        lat: jOwner.map.lat,
        lon: jOwner.map.long,
        radius: ownerRadius,
        name: jOwner.name,
        aP: jOwner.aP,
        lF: jOwner.lF,
        weather: jOwner.weather,
        type: jOwner.type
    };
}

function RegionConstruction(jRegion) {
    var regionRadius = jRegion.map.radius;
    if (regionRadius < 100000) {
        regionRadius = 100000;
    }
    regions['' + jRegion.name + ''] = {
        id: jRegion.id,
        lat: jRegion.map.lat,
        lon: jRegion.map.long,
        radius: regionRadius,
        color: '#FC7E3F',
        name: jRegion.name,
        aP: jRegion.aP,
        lF: jRegion.lF,
        weather: jRegion.weather,
        type: jRegion.type
    }
};

function ParkConstruction(jPark, loc, jAssets) {
    parksMap['' + jPark.name + ''] = {
        id: jPark.id,
        name: jPark.name,
        lat: jPark.map.lat,
        lon: jPark.map.long,
        loc: loc,
        parkType: jPark.parkType,
        type: jPark.type,
        aP: jPark.aP,
        lF: jPark.lF,
        radius: jPark.map.radius, //CircleRadiusCalculation(jPark.map.lat, jPark.map.long, jAssets),
        color: '#FC7E3F',
        assets: jAssets,
        kpis: jPark.kpis,
        iP: jPark.iP,
        mtdProd: jPark.mtdProd,
        ytdProd: jPark.ytdProd,
        weather: jPark.weather,
        avl: jPark.avl,
        wD: jPark.wD,
        wS: jPark.wS,
        eff: jPark.eff,
        type: jPark.type
    }
};

function AssetConstruction(jAssets, jAsset) {
    jAssets['' + jAsset.id + ''] = {
        id: jAsset.id,
        name: jAsset.id,
        lat: jAsset.lat,
        lon: jAsset.long,
        aP: jAsset.aP,
        lF: jAsset.lF,
        kpis: jAsset.kpis,
        type: jAsset.type,
        assetType: jAsset.assetType,
        kpis: jAsset.kpis,
        iP: jAsset.iP,
        parent: jAsset.parent,
        eff: jAsset.eff,
        status: jAsset.status,
        avl: jAsset.avl,
        wD: jAsset.wD,
        wS: jAsset.wS,
        airTemp: jAsset.airTemp,
        type: jAsset.type
    };
};

// Calculates the farthest asset to set the Circle Radius 
function CircleRadiusCalculation(parkLat, parkLon, jAssets) {
    var parkCoordinates = [parkLat, parkLon];
    var mostFaraway = 0,
        distance = 0;

    for (var ass in jAssets) {
        var coordinates = L.latLng(jAssets[ass].lat, jAssets[ass].lon);
        distance = coordinates.distanceTo(parkCoordinates);
        if (mostFaraway < distance) {
            mostFaraway = distance;
        }
    }
    if (mostFaraway === 0) {
        mostFaraway = 1000;
    }
    return mostFaraway;
};

function PolygonsConstruction() {
    for (var p in parksMap) {
        polygons['' + parksMap[p].name + ''] = {};
        var k = 0;

        for (var a in parksMap[p].assets) {
            polygons['' + parksMap[p].name + ''][k] = new L.LatLng(parksMap[p].assets[a].lat, parksMap[p].assets[a].lon);
            k++;
        }
    }
}

// Parks Insertion 
var parksPopup = [];
var ctxArrayParks = [];
function setParks() {

    var nodes_parks = tree_nodes.getNodesInLevel(4);
    var nodes_parks_count = nodes_parks.length;
    var node_park;
    var node_parent;
    var val_park;
    var val_park_id;
    var val_parent;
    var loc_park;
    var circle;

    for (var i = 0 ; i < nodes_parks_count ; i++) {
        node_park = nodes_parks[i];
        node_parent = node_park.parent;
        val_park = node_park.value;
        val_park_id = val_park.id;
        val_parent = node_parent.value;
        loc_park = val_park.loc;

        circle = new L.Icon.Canvas({
            iconSize: new L.Point(50, 50),
            id: val_park,
            className: 'parks-icons parks-icons-' + val_park_id
        });        

        if (typeof val_park.online == 'undefined')
            val_park.online = 0;

        if (typeof val_park.offline == 'undefined')
            val_park.offline = 0;

        if (typeof val_park.noComm == 'undefined')
            val_park.noComm = 0;

        var total = val_park.online + val_park.offline + val_park.noComm;
        var greenSize = 0;
        var redSize = 0;

        if (total != 'undefined' && total > 0) {
            greenSize = val_park.online * 2 / total;
            redSize = val_park.offline * 2 / total;
        }


        circle.draw = (function (item, total, greenSize, redSize) {
            return function (ctx, w, h) {
                ctxArrayParks.push({
                    id: item.id,
                    ctx: ctx
                });

                setUpParkForStatus(item, ctx, greenSize, redSize);

                switch (item.energyType) {
                    case 'WIND':
                        parkWindConstructor(ctx);
                        break;
                    case 'SOLAR':
                        parkSolarConstructor(ctx);
                        break;
                    case 'HYDRO':
                        parkHydroConstructor(ctx);
                        break;
                }
            };
        })(val_park, total, greenSize, redSize);

        var popup = L.popup({
            closeButton: false,
            closeOnClick: false,
            autoPan: false,
            className: 'leaflet-popup-park showLabel park-canvas-popup',
            id: val_park_id,
            type: 'park'
        })
            .setLatLng([loc_park.lat, loc_park.lon])
            .setContent('<div id="' + val_park_id + '" class="park-popup ' + val_park_id + 'POP popup" data-element="' + val_park_id + '" data-parent="' + val_parent.id + '" data-type="' + val_park.type + '" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; z-index: 400; top: 5px;  margin-left: -55px; margin-top: -20px;" href="callback:' + val_park_id + '"><div style="color: white; background-color: black; width: 141px; margin-left: 40px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 14px">' + val_park.name + '</span></div><div style="width: 146px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 19px"> LF: <span id="' + val_park_id + 'LF">' + Math.round(val_park.lf) + '</span>% <span style="padding-left: 14px">AP: <span id="' + val_park_id + 'AP">' + Math.round(val_park.ap) + '</span>mw</span></div></div>');

        popupsClick.push({
            id: val_park_id,
            popup: popup
        });

        var marker = new L.Marker([loc_park.lat, loc_park.lon], {
            icon: circle,
            draggable: false,
            opacity: 1,
            id: val_park_id,
            title: val_park_id,
            element: val_park,
            parent: val_parent.id,
            type: 'park',
            className: 'park-canvas-popup',
            zIndexOffset: 1000
        }).on('click', popupsManage)

        parksPopup.push({
            marker: marker,
            popup: popup,
            parkId: val_park_id,
            type: 'park'
        });

        marker.bindPopup(popup).addTo(map);

        markersClick.push({
            marker: marker,
            parkId: val_park,
            type: 'park'
        });



        /*switch (parksMap[park].parkType) {
        case 'WIND':
            eolics.addLayer(marker.bindPopup(popup));
            break;
        case 'SOLAR':
            solars.addLayer(marker.bindPopup(popup));

            break;
        case 'HYDRO':
            hydrics.addLayer(marker.bindPopup(popup));

            break;
        }*/
    }
    /*map.addLayer(eolics);
    map.addLayer(solars);
    map.addLayer(hydrics);*/

    parksExists = true;
};


function setOwnerCircle() {

    createCircle(owner);

    addedOwnerCircle = true;
}

function setCountryCircle() {

    var nodes_countries = tree_nodes.getNodesInLevel(2);
    var country_count = nodes_countries.length;
    var node_country;
    var val_country;

    for (var i = 0 ; i < country_count ; i++) {
        //createCircle(countries[country]);
        node_country = nodes_countries[i];
        val_country = node_country.value;
        addedCountryCircle = false;
        setCountries(node_country);
    }
}

function setCountries(node_country) {
    var country = node_country.value;

    var circle = new L.Icon.Canvas({
        iconSize: new L.Point(50, 50),
        className: 'country-icon country-icon-' + country.id
    });

    var node_parent = node_country.parent;
    var item = country;
    var country_id = country.id;
    var total = item.online + item.offline + item.nocomm;
    var greenSize = item.online * 2 / total;
    var redSize = item.offline * 2 / total;

    circle.draw = (function (item, total, greenSize, redSize) {
        return function (ctx, w, h) {

            ctxArray.push({
                id: item.id,
                ctx: ctx
            });

            var distance;
            if (country.acronym.length === 1) {
                distance = 17;
            } else {
                distance = 11;
            }
            setUpParkForStatus(item, ctx, greenSize, redSize);


            ctx.font = "16pt Helvetica-Th";
            ctx.fillStyle = "black";
            ctx.textAlign = "middle";
            ctx.fillText(country.acronym, distance, 35);

        };


    })(item, total, greenSize, redSize);

    var popup = L.popup({
        closeButton: false,
        closeOnClick: false,
        autoPan: false,
        className: 'leaflet-popup-country showLabel country-canvas-popup',
        id: country_id,
    })
        .setLatLng([country.loc.lat, country.loc.lon])
        .setContent('<p><div id="' + country_id + 'POP" class="country-popup popup" data-type="' + country.type + '" data-element="' + country_id + '" data-parent="' + node_parent.id + '"  style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; margin-left: -50px; margin-top: -25px; z-index: 400; top: 20px" href="callback:' + country_id + '"><div style="color: white; background-color: black; width: 140px; margin-left: 37px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 20px">' + country.name + '</span></div><div style="width: 143px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 22px"> LF: <span id="' + country_id + 'LF">' + Math.round(country.lf) + '</span>% <span style="padding-left: 14px">AP: <span id="' + country.id + 'AP">' + Math.round(country.ap) + '</span>MW</span></div></div>');

    popupsClick.push({
        id: country_id,
        popup: popup
    });

    var marker = new L.Marker([country.loc.lat, country.loc.lon], {
        icon: circle,
        draggable: false,
        opacity: 1,
        id: country_id,
        title: country_id,
        element: country,
        className: 'country-canvas-popup'
    }).bindPopup(popup)
    .on('click', popupsManage)
    .addTo(map);

    markersClick.push({
        marker: marker
    });
}

var assetPopup = [];
// Assets insertion 
function setAssets() {

    var nodes_parks = tree_nodes.getNodesInLevel(4);
    var park_count = nodes_parks.length;
    var node_park;
    var val_park;
    var park_id;
    var nodes_park_assets;
    var nodes_park_assets_count;
    var node_asset;
    var val_asset;
    var asset_id;

    //for (var park in parksMap) {
    for (var i = 0 ; i < park_count ; i++) {

        node_park = nodes_parks[i];
        val_park = node_park.value;
        park_id = val_park.id;
        nodes_park_assets = node_park.children;
        nodes_park_assets_count = node_park.children.length;

        //for (var asset in parksMap[park].assets) {
        for (var asset_i = 0 ; asset_i < nodes_park_assets_count ; asset_i++) {
            node_asset = nodes_park_assets[asset_i];
            val_asset = node_asset.value;
            asset_id = val_asset.id;

            var color;

            switch (val_asset.status) {
                case 100: // run
                    color = '#39B54A';
                    break;
                case 200: // ready
                    color = '#0071BC';
                    break;
                case 300: // stop
                    color = '#C1272D';
                    break;
                case 400: // maint
                    color = '#FCEE21';
                    break;
                case 500: // restricted
                    color = '#F7931E';
                    break;
                case 600:
                case 301: // repair
                    color = '#A67C52';
                    break;
                default: // nocomm
                    color = '#D927BF';
                    break;
            }

            var circle = new L.Icon.Canvas({
                iconSize: new L.Point(50, 50),
                className: 'asset-icons asset-' + val_park.id + '-' + asset_id + ' asset-icons-' + val_park.id,
            });

            var item = val_asset;
            var itemPark = val_park;

            circle.draw = (function (item, itemPark, color) {
                return function (ctx, w, h) {
                    ctx.clearRect(0, 0, 50, 50);
                    drawArc(ctx, 0, 2, color);

                    ctxArray.push({
                        assetID: item.id,
                        parkID: itemPark.id,
                        ctx: ctx
                    });

                    switch (itemPark.energyType) {
                        case 'WIND':
                            assetWindConstructor(ctx);
                            break;
                        case 'SOLAR':
                            assetSolarConstructor(ctx)
                            break;
                        case 'HYDRO':
                            assetHydroConstructor(ctx)
                            break;

                    }
                }
            })(item, itemPark, color);

            // Calculates the popup width by the LF and AP values
            var width1, width2;
            var lF = Math.round(val_asset.lf);
            var aP = Math.round(val_asset.ap);

            if (lF.lenght > 2 || aP.lenght > 2) {
                width1 = '128px';
                width2 = '148px';
            }
            else {
                width1 = '122px';
                width2 = '142px';
            }
            
            var popup = L.popup({
                closeButton: false,
                closeOnClick: false,
                autoPan: false,
                className: 'asset-popup',
                id: asset_id,
                parkId: park_id,
                type: 'asset'
            })
                .setLatLng([val_asset.loc.lat, val_asset.loc.lon])
                .setContent('<p><div id="' + asset_id + 'POP" class="asset-marker popup" data-type="asset" data-element="' + asset_id + '" data-parent="' + park_id + '" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: ' + width1 + ' ; position: absolute; top: 14px; left: 20px;  margin-left: -35px;" href="callback: park:' + park_id + ', asset:' + asset_id + '"><div id=' + park_id + asset_id + 'BAR style="color: white; background-color:' + color + '; width: ' + width2 + ' ; margin-top: -16px; padding-top: 1px; margin-left: 6px;"><span style="padding-left: 16px">' + asset_id + '</span></div><div style="width: ' + width1 + ' ; background-color: white; font-size: 10.2px; margin-left: 6px; padding-left: 20px;">LF: <span id="' + park_id + asset_id + 'LF">' + lF + '</span>% <span style="padding-left: 14px">AP: <span id="' + park_id + asset_id + 'AP">' + aP + '</span>mw </span></span></div></div>')
                //.setContent('<p><div id="' + asset_id + 'POP" class="asset-marker popup" data-type="asset" data-element="' + asset_id + '" data-parent="' + park_id + '" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 128px; position: absolute; top: 14px; left: 20px;  margin-left: -35px;" href="callback: park:' + park_id + ', asset:' + asset_id + '"><div id=' + park_id + asset_id + 'BAR style="color: white; background-color:' + color + '; width: 148px; margin-top: -16px; padding-top: 1px; margin-left: 6px;"><span style="padding-left: 16px">' + asset_id + '</span></div><div style="width: 128px; background-color: white; font-size: 10.2px; margin-left: 6px; padding-left: 20px;">LF: <span id="' + park_id + asset_id + 'LF">' + Math.round(val_asset.lf) + '</span>% <span style="padding-left: 14px">AP: <span id="' + park_id + asset_id + 'AP">' + Math.round(val_asset.ap) + '</span>mw </span></span></div></div>')

            popupsClick.push({
                popup: popup,
                assetID: asset_id,
                parkID: park_id
            });

            var marker = new L.Marker([val_asset.loc.lat, val_asset.loc.lon], {
                icon: circle,
                draggable: false,
                opacity: 1,
                id: asset_id,
                title: asset_id,
                element: val_asset,
            }).on('click', popupsManage)

            assetPopup.push({
                marker: marker,
                popup: popup,
                type: 'asset',
                assetId: asset_id,
                parkId: park_id
            });

            marker.bindPopup(popup).addTo(map);

            //assets.addLayer(marker.bindPopup(popup));

            markersClick.push({
                marker: marker,
                type: 'asset',
                assetId: asset_id,
                parkId: park_id
            });

        }
    }
    addedAssets = false;
    //map.addLayer(assets);
}

// Set Parks' Circles 
function setParksCircles() {
    if (addedParkCircles != true) {

        for (var park in parksMap) {
            createCircle(parksMap[park]);
        }

        addedParkCircles = true;
    }
}

// Set Regions' Circles 
function setRegionCircle() {
    if (addedRegionCircles != true) {
        regionCirclesLayer = new L.layerGroup();

        var nodes_regions = tree_nodes.getNodesInLevel(3);
        var regions_count = nodes_regions.length;
        //for (var reg in regions) {
        for (var i = 0 ; i < regions_count ; i++) {

            setRegions(nodes_regions[i]);
            //createCircle(regions[reg]);
        }

        addedRegionCircles = true;
    }
}

function setRegions(node_region) {
    var region = node_region.value;
    var region_id = region.id;

    var circle = new L.Icon.Canvas({
        iconSize: new L.Point(50, 50),
        className: 'region-icon region-icon-' + region_id
    });

    var node_parent = node_region.parent;
    var val_parent = node_parent.value;

    var parent_id = val_parent.id;
    var item = region;
    var total = item.online + item.offline + item.nocomm;
    var greenSize = item.online * 2 / total;
    var redSize = item.offline * 2 / total;

    circle.draw = (function (item, total, greenSize, redSize) {
        return function (ctx, w, h) {

            ctxArray.push({
                id: item.id,
                ctx: ctx
            });

            var distance;
            if (region.acronym.length === 1) {
                distance = 17;
            } else {
                distance = 11;
            }
            setUpParkForStatus(item, ctx, greenSize, redSize);


            ctx.font = "16pt Helvetica-Th";
            ctx.fillStyle = "black";
            ctx.textAlign = "middle";
            ctx.fillText(region.acronym, distance, 35);

        };
    })(item, total, greenSize, redSize);

    var popup = L.popup({
        closeButton: false,
        closeOnClick: false,
        autoPan: false,
        className: 'leaflet-popup-region showLabel region-canvas-popup',
        id: region_id,
        type: 'region'
    })
        .setLatLng([region.loc.lat, region.loc.lon])
        .setContent('<p><div id="' + region_id + 'POP" class="region-popup popup" data-type="' + region.type + '" data-element="' + region_id + '" data-parent="' + val_parent.id + '" style="display: none; box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative;  margin-left: -50px; margin-top: -25px; z-index: 400; top: 20px" href="callback:' + region_id + '"><div style="color: white; background-color: black; width: 140px; margin-left: 37px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 20px">' + region.name + '</span></div><div style="width: 143px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 22px"> LF: <span id="' + region_id + 'LF">' + Math.round(region.lf) + '</span>% <span style="padding-left: 14px">AP: <span id="' + region_id + 'AP">' + Math.round(region.ap) + '</span>MW</span></div></div>');

    popupsClick.push({
        id: region_id,
        popup: popup
    });

    var marker = new L.Marker([region.loc.lat, region.loc.lon], {
        icon: circle,
        draggable: false,
        opacity: 1,
        id: region_id,
        title: region_id,
        element: region,
        className: 'regions-canvas-popup'
    }).bindPopup(popup)
    .on('click', popupsManage)
    .addTo(map);

    markersClick.push({
        marker: marker
    });
};

// Parks' Polygons 
function setPolygons() {
    PolygonsConstruction();
    multiPolygon = [];

    for (var poly in polygons) {
        var pointsCount = 0;

        for (var point in polygons[poly]) {
            pointsCount++;
        }

        var polygonPoints = [];

        for (var j = 0; j < pointsCount; j++) {
            polygonPoints.push(polygons[poly][j]);
        }

        multiPolygon.push(polygonPoints);
    }

    polygonGroup = new L.multiPolygon(multiPolygon, {
        color: '#FC7E3F',
        weight: 3,
        className: 'park-polygon'
    });
    map.addLayer(polygonGroup);

    addedPolygons = true;
}

function zoomDefinitions(SetAssetsZoom, SetParkRegionCircleMin, SetParkRegionCircleMax, SetCountryCircleMin, SetCountryCircleMax, OwnerCircleMin, OwnerCircleMax) {

    setAssetsZoom = SetAssetsZoom;
    setParkRegionCircleMin = SetParkRegionCircleMin;
    setParkRegionCircleMax = SetParkRegionCircleMax;
    setCountryCircleMin = SetCountryCircleMin;
    setCountryCircleMax = SetCountryCircleMax;
    ownerCircleMin = OwnerCircleMin;
    ownerCircleMax = OwnerCircleMax;

    $('.owner-label').css("display", "inline-block");
    $('.test-label').css("display", "none");
    $('.test-label-' + owner.id).css("display", "inline-block");
    $('.owner-canvas-popup').css("display", "inline-block");
    $('.country-icon').css("display", "inline-block");

    var zoomLevel = GetZoomLevel();

    if (zoomLevel > setCountryCircleMax || zoomLevel <= setCountryCircleMin) {
        $('.country-label').css("display", "none");
    }

    if (zoomLevel >= setParkRegionCircleMax || zoomLevel <= setParkRegionCircleMin) {
        $('.region-label').css("display", "none");
        $('.region-icon').css("display", "none");
    }

    if (zoomLevel <= setParkRegionCircleMax) {
        $('.parks-icons').css("display", "none");
        $('.park-label').css("display", "none");
    }

    //$('.asset-icons').css("display", "none");
    //map.removeLayer(assets);

    $('.park-polygon').css("display", "none");
    $('.asset-icons').css("display", "none");

    map.on('zoomend', onZoomEnd);
    map.on('dragend', setAnglesZoom);
    map.on('popupopen', function (e) {
        $('.leaflet-marker-pane').append($('.leaflet-popup-pane'));
    })
}

//var parkIDShow;
var park_selected = null;
// On zoom 10 writes down park circles 
function onZoomEnd(e) {
    var zoomLevel = GetZoomLevel();

    //$('.park-popup').css('visibility', 'hidden');
    map.closePopup();

    if (zoomLevel > ownerCircleMax) {
        $('.country-icon').css("display", "none");
        $('.country-popup').css("display", "none");

        $('.owner-label').css("display", "none");
        $('.owner-label').css("z-index", "-1");
        $('.owner-label').remove();
        $('.test-label').remove();

        addedOwnerCircle = false;
    }

    if (zoomLevel <= ownerCircleMax) {
        node_selected = tree_nodes.findNodeInLevel(1, owner);

        $('.owner-label').css("display", "inline-block");
        $('.country-icon').css("display", "inline-block");
        $('.country-icon').css("zIndex", "10000");
        $('.owner-label').css("z-index", "-1");
        $('.test-label').css("z-index", "10");
        /*map.removeLayer(eolics);
        map.removeLayer(solars);
        map.removeLayer(hydrics);*/

        addedOwnerCircle = true;

        $('.owner-label').remove();
        $('.test-label-owner').remove();
        createCircle(owner);
    }

    if (zoomLevel >= setCountryCircleMax || zoomLevel <= setCountryCircleMin) {
        $('.country-label').css("display", "none");
        $('.region-icon').css("display", "none");
        $('.region-popup').css("display", "none");

        addedCountryCircle = false;
        $('.country-label').remove();
        $('.test-label-country').remove();
    }

    if (zoomLevel > setCountryCircleMin && zoomLevel < setCountryCircleMax) {
        $('.country-label').css("display", "inline-block");
        $('.region-icon').css("display", "inline-block");
        $('.region-icon').css("zIndex", "10000");

        /*map.removeLayer(eolics);
        map.removeLayer(solars);
        map.removeLayer(hydrics);*/

        $('.country-label').remove();
        $('.test-label-country').remove();

        var nodes_country = tree_nodes.getNodesInLevel(2);
        var nodes_country_count = nodes_country.length;
        for (var country_i = 0; country_i < nodes_country_count ; country_i++) {
            createCircle(nodes_country[country_i].value);
        }
        //for (var country in countries) {
        //    createCircle(countries[country]);
        //}
        addedCountryCircle = true;
    }

    if (zoomLevel > setParkRegionCircleMax || zoomLevel < setParkRegionCircleMin) {


        $('.region-label').css("display", "none");
        $('.parks-icons').css("display", "none");

        $('.region-label').remove();
        $('.test-label-region').remove();
        addedRegionCircles = false;
    }

    if (zoomLevel >= setParkRegionCircleMin && zoomLevel <= setParkRegionCircleMax) {
        $('.region-label').css("display", "inline-block");
        $('.parks-icons').css("display", "inline-block");
        $('.parks-icons').css("zIndex", "10000");

        /*map.addLayer(eolics);
        map.addLayer(solars);
        map.addLayer(hydrics);*/

        $('.region-label').remove();
        $('.test-label-region').remove();

        if (zoomLevel <= 9) {
            var nodes_regions = tree_nodes.getNodesInLevel(3);
            var nodes_regions_count = nodes_regions.length;
            for (var region_i = 0 ; region_i < nodes_regions_count ; region_i++) {
                createCircle(nodes_regions[region_i].value);
            }
            //for (var reg in regions) {
            //    createCircle(regions[reg]);
            //}
        }

        /*for(var ap = 0; ap < parksPopup.length; ap++) {
                //console.log(map.getBounds().contains(parksPopup[ap].marker.getPosition));
            }*/

        addedRegionCircles = true;

    }

    if (zoomLevel > setAssetsZoom) {
        //$('park-label').css("display", "inline-block");

        //assets.clearLayers();
        //setAssets();

        //$('.asset-icons').remove();

        $('.park-label').remove();
        $('.test-label-park').remove();

        var mapCenter = map.getCenter();
        var nodes_parks = tree_nodes.getNodesInLevel(4);
        var node_park_selected = findClosestFromCoordinates(nodes_parks, mapCenter);
        park_selected = node_park_selected.value;

        if (zoomLevel < 16) {
            createCircle(park_selected);
        }

        $('.asset-icons-' + park_selected.id).css('display', 'inline-block');

        //for (var ap = 0; ap < assetPopup.length; ap++) {
        //    if (assetPopup[ap].parkId === park_selected.id) {
        //        //map.addLayer(assetPopup[ap].marker.bindPopup(assetPopup[ap].popup));
        //        $('.asset-icons-' + park_selected.id).css('display', 'inline-block');
        //    }
        //}

        $('.parks-icons').css('display', 'inline-block');
        $('.park-label-' + park_selected.id).css('display', 'inline-block');
        $('.test-label-park-' + park_selected.id).css('display', 'inline-block');
        $('.parks-icons-' + park_selected.id).css('display', 'none');


        //$('.asset-icons').css('display', 'none');
        //$('.asset-icons-' + parkIDShow).css('display', 'inline-block');
        //$('.asset-icons-' + parkIDShow).css('zIndex', '1000')
        $('.park-popup').css('visibility', 'hidden');

        //$('.park-polygon').css("display", "inline-block");


        parksExists = true;
        addedAssets = true;
    }

    if (park_selected !== null && zoomLevel <= setAssetsZoom) {

        //for (var k = 0; k < markersClick.length; k++) {
        //    if (markersClick[k].type === 'asset') {
        //        markersClick[k].marker.closePopup();
        //    }
        //}


        //map.removeLayer(assets);

        //$('.asset-icons').css("display", "none");
        //$('.asset-icons').remove();


        $('.park-polygon').css("display", "none");
        $('.park-label').css("display", "none");
        $('.park-label').remove();
        $('.test-label-park').remove();

        $('.asset-icons').css('display', 'none');

        //for (var ap = 0; ap < assetPopup.length; ap++) {
        //    if (assetPopup[ap].parkId === park_selected.id) {
        //        //map.removeLayer(assetPopup[ap].marker);
        //        $('.asset-icons').css('display', 'none');
        //    }
        //}

        //$('.park-circle').css("display", "none");
        //$('.park-circle-popup').css("display", "none");



        addedAssets = false;
    }

    //ChangeZIndex();

}

function findClosestFromCoordinates(nodes, coords) {
    var result = null;
    var node_curr = null;
    var val_curr = null;
    var minDistance = 0;
    var nodes_count = nodes.length;
    for (var i = 0 ; i < nodes_count ; i++) {
        node_curr = nodes[i];
        val_curr = node_curr.value;
        var coordinates = L.latLng(val_curr.loc.lat, val_curr.loc.lon)
        if (minDistance === 0) {
            minDistance = coordinates.distanceTo(coords);
            result = node_curr;
        }

        if (minDistance > coordinates.distanceTo(coords)) {
            minDistance = coordinates.distanceTo(coords);
            result = node_curr;
        }
    }
    return result;
}

function setAnglesZoom(e) {

    if (map.getZoom() > setAssetsZoom) {
        $('.parks-icons').css("zIndex", "10000");
        $('.popup').css("display", "none");
        $('.parks-icons-' + park_selected.id).css('display', 'inline-block');

        $('.park-label-' + park_selected.id).remove();
        //$('.asset-icons').remove();
        //$('.asset-icons').css('display', 'none');
        $('.test-label-park-' + park_selected.id).remove();

        for (var ap = 0; ap < assetPopup.length; ap++) {
            if (assetPopup[ap].parkId === park_selected.id) {
                //map.removeLayer(assetPopup[ap].marker);
                $('.asset-icons-' + park_selected.id).css('display', 'none');
            }
        }

        var mapCenter = map.getCenter();
        var nodes_parks = tree_nodes.getNodesInLevel(4);
        var node_park_selected = findClosestFromCoordinates(nodes_parks, mapCenter);
        park_selected = node_park_selected.value;

        if (map.getZoom() < 16) {
            createCircle(park_selected);
        }

        for (var ap = 0; ap < assetPopup.length; ap++) {
            if (assetPopup[ap].parkId === park_selected.id) {
                //map.addLayer(assetPopup[ap].marker.bindPopup(assetPopup[ap].popup));
                $('.asset-icons-' + park_selected.id).css('display', 'inline-block');
            }
        }

        $('.park-label-' + park_selected.id).css('display', 'inline-block');
        $('.test-label-park-' + park_selected.id).css('display', 'inline-block');
        $('.parks-icons-' + park_selected.id).css('display', 'none');

        //$('.asset-marker').css('display', 'none');


        $('.asset-icons-' + park_selected.id).css('display', 'inline-block');
        //$('.park-polygon').css("display", "inline-block");
    }
};

// Get Current zoom level 
function GetZoomLevel() {
    return map.getZoom();
};

var ctxCircleLabels = [];
function createCircle(item) {
    var valueTranslate;
    switch (map.getZoom()) {
        case 4:
            valueTranslate = 40;
            break;
        case 5:
            valueTranslate = 80;
            break;
        case 6:
            valueTranslate = 160;
            break;
        case 7:
            valueTranslate = 320;
            break;
        case 8:
            valueTranslate = 640;
            break;
        case 9:
            valueTranslate = 1240;
            break;
        case 10:
            valueTranslate = 2560;
            break;
        case 11:
            valueTranslate = 5120;
            break;
        case 12:
            valueTranslate = 10240;
            break;
        case 13:
            valueTranslate = 20480;
            break;
        case 14:
            valueTranslate = 40960;
            break;
        case 15:
            valueTranslate = 81920;
            break;
        case 16:
            valueTranslate = 163840;
            break;
        case 17:
            valueTranslate = 327680;
            break;
        case 18:
            valueTranslate = 655360;
            break;
    }

    var markerCircle;
    var classNames;
    var label;

    if (item.type === 'owner') {
        //$('.owner-label').remove();

        label = new L.Icon.Canvas({
            iconSize: new L.Point(12000, 1000),
            className: 'owner-label'
        });

        classNames = 'owner-canvas-popup'
    } else if (item.type === 'country') {
        //$('.country-label').remove();

        label = new L.Icon.Canvas({
            iconSize: new L.Point(12000, 1000),
            className: 'country-label country-label-' + item.id
        });

        classNames = 'country-canvas-popup'
    } else if (item.type === 'region') {
        //$('.region-label').remove();

        label = new L.Icon.Canvas({
            iconSize: new L.Point(1000, 600),
            className: 'region-label region-label-' + item.id
        });

        classNames = 'region-canvas-popup'
    } else {
        //$('.park-label').remove();

        label = new L.Icon.Canvas({
            iconSize: new L.Point(1000, 600),
            className: 'park-label park-label-' + item.id
        });

        classNames = 'park-canvas-popup'
    }

    var valueRad = (item.radius * valueTranslate) / 300000;
    var item = item;

    if (valueRad <= 400 || item.type === 'park') {
        if (valueRad < 400 && item.type === 'park' || valueRad <= 400) {
            label.draw = (function (item) {
                return function (ctx, w, h) {
                    //ctx.clearRect(0, 0, 12000, 1000);

                    var x = (w / 2);
                    var y = (h / 2);
                    var radius = valueRad;
                    var startAngle = 1.99 * Math.PI;
                    var endAngle = 0.01 * Math.PI;
                    var counterClockwise = false;

                    var xCalc = x + radius + 15;
                    var yCalc = y - 15;

                    var topX = x;
                    var topY = y - 15;

                    var valuea = Math.sqrt(Math.pow(radius, 2) - Math.pow(15, 2));

                    ctx.beginPath();

                    ctx.moveTo(x + valuea, y - 15);
                    ctx.lineTo(x + radius + 15, y - 15);

                    ctx.arc(x, y, radius, 0 * Math.PI, startAngle, true);

                    ctx.moveTo(x + radius + 15, y - 15);
                    ctx.lineTo(x + radius + 15, y);

                    ctx.fillStyle = '#FC7E3F';
                    ctx.strokeStyle = '#FC7E3F';
                    ctx.fill();
                    ctx.stroke();
                    ctx.closePath();

                    ctx.beginPath();
                    ctx.moveTo(x + valuea, y + 15);
                    ctx.lineTo(x + radius + 15, y + 15);

                    ctx.arc(x, y, radius, 0 * Math.PI, endAngle, false);

                    ctx.moveTo(x + radius + 15, y + 15);
                    ctx.lineTo(x + radius + 15, y);

                    ctx.strokeStyle = '#ffffff';

                    ctx.strokeStyle = '#ffffff';
                    ctx.fillStyle = '#ffffff';
                    ctx.fill();
                    ctx.stroke();
                    ctx.closePath();

                    ctx.beginPath();
                    ctx.arc(x, y, radius, 0 * Math.PI, endAngle, false);
                    ctx.strokeStyle = '#FC7E3F';
                    ctx.stroke();
                    ctx.closePath();

                    ctx.beginPath();
                    ctx.rect(x + radius, y - 16, 150, 15);
                    ctx.fillStyle = '#FC7E3F';
                    ctx.fill();

                    ctx.lineWidth = 1;
                    ctx.strokeStyle = '#FC7E3F';
                    ctx.stroke();

                    ctx.beginPath();
                    ctx.rect(x + radius + 2, y, 148, 15);
                    ctx.fillStyle = '#ffffff';
                    ctx.fill();
                    ctx.lineWidth = 1;
                    ctx.strokeStyle = '#ffffff';
                    ctx.stroke();

                    ctx.beginPath();
                    ctx.arc(x, y, radius, 0 * Math.PI, 2 * Math.PI, counterClockwise);
                    ctx.lineWidth = 2;

                    // line color
                    ctx.strokeStyle = '#FC7E3F';
                    ctx.stroke();
                    ctx.closePath();

                }
            })(item);

            markerCircle = new L.Marker([item.loc.lat, item.loc.lon], {
                icon: label,
                draggable: false,
                opacity: 1,
                id: item.id,
                title: item.id,
                element: item,
                className: classNames
            }).addTo(map);

            markerCircle._icon.style.zIndex = '-1';

            var classPark = '';
            if (item.type === 'park') {
                classPark = 'test-label-park-' + item.id;
            }

            var testa = new L.Icon.Canvas({
                iconSize: new L.Point(180, 31),
                className: 'test-label test-label-' + item.id + ' test-label-' + item.type + '  ' + classPark
            });

            testa.draw = (function (item) {
                return function (ctx, w, h) {

                    ctx.font = '12pt Helvetica-Cn';
                    ctx.fillStyle = '#ffffff';
                    ctx.fillText(item.name, 20, 14);

                    ctx.closePath();

                    ctx.font = '9pt Helvetica-LtCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText('LF: ', 20, 29);

                    ctx.font = '10pt Helvetica-BdCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText(Math.round(item.lf), 40, 29);

                    ctx.font = '9pt Helvetica-BdCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText('%', 60, 29);

                    ctx.font = '9pt Helvetica-LtCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText('AP: ', 80, 29);

                    ctx.font = '10pt Helvetica-BdCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText(Math.round(item.ap), 100, 29);

                    ctx.font = '9pt Helvetica-BdCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText('mW', 125, 29);

                    ctx.closePath();

                    if (item.type !== 'owner' && item.type !== 'region' && item.type !== 'country') {
                        ctx.beginPath();
                        ctx.rect(150, 0, 32, 32);
                        ctx.fillStyle = '#FC7E3F';
                        ctx.fill();
                        ctx.closePath();

                        ctx.beginPath();
                        ctx.font = '18pt Meteo';
                        ctx.fillStyle = '#ffffff';
                        ctx.fillText(getWeather(item.weather), 153, 27);
                        ctx.closePath();

                    }

                    ctxCircleLabels.push({
                        ctx: ctx,
                        item: item,
                        id: item.id
                    });

                }
            })(item);

            var m = new L.Marker([item.loc.lat, item.loc.lon], {
                icon: testa,
                draggable: false,
                opacity: 1,
                id: item.id,
                title: item.id,
                element: item,
                className: classNames
            }).addTo(map);

            m._icon.style.zIndex = '7';

            $('.test-label-' + item.id)[0].style.marginLeft = valueRad;
            $('.test-label-' + item.id)[0].style.marginTop = '-17px';
            $('.test-label-' + item.id)[0].dataset.element = item.id;
        }
    } else {
        if (item.type !== 'park') {
            createMultipleCircles(item);
        }
    }
};

function createMultipleCircles(item) {

    var groups = [];
    var checkedPark = [];
    var latAvg = 0;
    var lonAvg = 0;
    var numberOfElements = 0;
    var distanceForRadius = 0;

    var nodes_parks = tree_nodes.getNodesInLevel(4);


    nodes_parks.forEach(function (n) {
        var p = n.value;
        var p_parent = n.parent.value;
        nodes_parks.forEach(function (n1) {
            var p1 = n1.value;
            var p1_parent = n1.parent.value;

            if (p.id !== p1.id && item.id === p_parent.id && item.id === p1_parent.id) {
                if (checkedPark.length === 0) {
                    checkedPark.push(p.id);
                } else {

                    if (groups.indexOf(p.id) === -1) {
                        var coordinates = L.latLng(p.loc.lat, p.loc.lon);
                        var parkCoordinates = L.latLng(p1.loc.lat, p1.loc.lon);
                        distance = coordinates.distanceTo(parkCoordinates);
                        if (distance > 200000) {
                            if (groups.indexOf(p1.id) === -1) {
                                groups.push(p1.id);
                            }
                        } else {
                            if (distanceForRadius < distance) {
                                distanceForRadius = distance - 10000;
                            }
                        }
                    }
                }
            }
        })
        groups.forEach(function (g) {
            if (p.id !== g) {
                latAvg = latAvg + p.loc.lat;
                lonAvg = lonAvg + p.loc.lon;
                numberOfElements++;
            }
        })
    })
    latAvg = latAvg / numberOfElements;
    lonAvg = lonAvg / numberOfElements;

    var v = checkedPark.concat(groups);

    var originalRadius = item.radius;
    var originalLat = item.loc.lat;
    var originalLon = item.loc.lon;
    var originalId = item.id;
    var tmp_node;
    var tmp_park;
    var count = 0;
    if (groups.length > 0) {
        v.forEach(function (v) {
            var newItem = item;
            if (groups.indexOf(v) !== -1) {
                tmp_node = tree_nodes.findNodeInLevel(4, v);
                tmp_park = tmp_node.value;
                newItem.loc.lat = tmp_park.loc.lat;
                newItem.loc.lon = tmp_park.loc.lon;
                newItem.radius = 40000;
            } else {
                newItem.loc.lat = latAvg;
                newItem.loc.lon = lonAvg;
                newItem.radius = distanceForRadius
            }
            newItem.id = originalId + '-' + count;
            count++;
            createCircle(newItem);
        });
        item.radius = originalRadius;
        item.loc.lat = originalLat;
        item.loc.lon = originalLon;
        item.id = originalId;
    }
}

var maxParksGeneral;
function createCluster(maxParks) {
    maxParksGeneral = maxParks;
    eolics = new L.MarkerClusterGroup({
        spiderfyOnMaxZoom: false,
        zoomToBoundsOnClick: false,
        disableClusteringAtZoom: maxParks,
        showCoverageOnHover: false,
        maxClusterRadius: 130,
        iconCreateFunction: function (cluster) {
            return L.divIcon({
                className: 'marker-cluster marker-cluster-small',
                html: '<div class="marker-cluster-eolicPark eolicParkNoBack" style="border: 4px solid #cccccc;"><span>' + cluster.getChildCount() + '</span></div>'
            });
        }
    });

    solars = new L.MarkerClusterGroup({
        spiderfyOnMaxZoom: false,
        zoomToBoundsOnClick: false,
        disableClusteringAtZoom: maxParks,
        showCoverageOnHover: false,
        maxClusterRadius: 130,
        iconCreateFunction: function (cluster) {
            return L.divIcon({
                className: 'marker-cluster marker-cluster-small',
                html: '<div id="solarsCount"><span>' + cluster.getChildCount() + '</span></div>'
            });
        }
    });
    hydrics = new L.MarkerClusterGroup({
        spiderfyOnMaxZoom: false,
        zoomToBoundsOnClick: false,
        disableClusteringAtZoom: maxParks,
        showCoverageOnHover: false,
        maxClusterRadius: 130,
        iconCreateFunction: function (cluster) {
            return L.divIcon({
                className: 'marker-cluster marker-cluster-small',
                html: '<div id="hydricsCount"><span>' + cluster.getChildCount() + '</span></div>'
            });
        }
    });
}

function updateOwner(id, lf, ap) {
    owner.ap = ap;
    owner.lf = lf;

    if (map.getZoom() <= ownerCircleMax) {
        var ctx_circle_count = ctxCircleLabels.length;
        var ctx_circle_arr_lbl, ctx_circle_lbl;
        for (var z = 0; z < ctx_circle_count; z++) {
            ctx_circle_arr_lbl = ctxCircleLabels[z];
            if (ctx_circle_arr_lbl.id.substring(0, 3) === id) {
                ctx_circle_lbl = ctx_circle_arr_lbl.ctx;
                ctx_circle_lbl.clearRect(0, 0, 160, 30);

                ctx_circle_lbl.font = '12pt Helvetica-Cn';
                ctx_circle_lbl.fillStyle = '#ffffff';
                ctx_circle_lbl.fillText(owner.name, 20, 14);

                ctx_circle_lbl.closePath();

                ctx_circle_lbl.font = '9pt Helvetica-LtCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('LF: ', 20, 29);

                ctx_circle_lbl.font = '10pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText(Math.round(owner.lf), 40, 29);

                ctx_circle_lbl.font = '9pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('%', 60, 29);

                ctx_circle_lbl.font = '9pt Helvetica-LtCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('AP: ', 80, 29);

                ctx_circle_lbl.font = '10pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText(Math.round(owner.ap), 100, 29);

                ctx_circle_lbl.font = '9pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('mW', 125, 29);

                ctx_circle_lbl.closePath();
            }
        }
    }
}

function updateCountry(id, lf, ap, online, offline, nocomm, weather) {

    var node_country = tree_nodes.findNodeInLevel(2, id);
    var country = node_country.value;
    var country_id = country.id;
    //for (var country in countries) {
    //    if (countries[country].id === id) {
    country.ap = ap;
    country.lf = lf;
    country.weather = weather;
    country.online = online;
    country.offline = offline;
    country.nocomm = nocomm;

    var ctx_count = ctxArray.length;
    var ctx_arr_curr, ctx_curr;
    for (var i = 0; i < ctx_count; i++) {
        ctx_arr_curr = ctxArray[i];
        ctx_curr = ctx_arr_curr.ctx;
        if (ctx_arr_curr.id === id) {
            var distance;
            if (country.acronym.length === 1) {
                distance = 17;
            } else {
                distance = 11;
            }

            //var item = country;
            var total = country.online + country.offline + country.nocomm;
            var greenSize = country.online * 2 / total;
            var redSize = country.offline * 2 / total;

            setUpParkForStatus(country, ctx_curr, greenSize, redSize);


            ctx_curr.font = "16pt Helvetica-Th";
            ctx_curr.fillStyle = "black";
            ctx_curr.textAlign = "middle";
            ctx_curr.fillText(country.acronym, distance, 35);
        }
    }

    if (map.getZoom() > setCountryCircleMin && map.getZoom() < setCountryCircleMax) {
        var ctx_circle_count = ctxCircleLabels.length;
        var ctx_circle_arr_lbl;
        var ctx_circle_lbl;
        for (var z = 0; z < ctx_circle_count; z++) {
            ctx_circle_arr_lbl = ctxCircleLabels[z];
            ctx_circle_lbl = ctxCircleLabels[z].ctx;
            if (ctx_circle_arr_lbl.id.substring(0, 3) === id) {
                ctx_circle_lbl.clearRect(0, 0, 160, 30);

                ctx_circle_lbl.font = '12pt Helvetica-Cn';
                ctx_circle_lbl.fillStyle = '#ffffff';
                ctx_circle_lbl.fillText(country.name, 20, 14);

                ctx_circle_lbl.closePath();

                ctx_circle_lbl.font = '9pt Helvetica-LtCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('LF: ', 20, 29);

                ctx_circle_lbl.font = '10pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText(Math.round(country.lf), 40, 29);

                ctx_circle_lbl.font = '9pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('%', 60, 29);

                ctx_circle_lbl.font = '9pt Helvetica-LtCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('AP: ', 80, 29);

                ctx_circle_lbl.font = '10pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText(Math.round(country.ap), 100, 29);

                ctx_circle_lbl.font = '9pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('mW', 125, 29);

                ctx_circle_lbl.closePath();

                ctx_circle_lbl.beginPath();
                ctx_circle_lbl.rect(150, 0, 32, 32);
                ctx_circle_lbl.fillStyle = '#FC7E3F';
                ctx_circle_lbl.fill();
                ctx_circle_lbl.closePath();

                ctx_circle_lbl.beginPath();
                ctx_circle_lbl.font = '18pt Meteo';
                ctx_circle_lbl.fillStyle = '#ffffff';
                ctx_circle_lbl.fillText(getWeather(country.weather), 153, 27);
                ctx_circle_lbl.closePath();
            }
        }

    }

    if ($('.country-canvas-popup').length === 0) {
        var popups_click_count = popupsClick.length;
        var popup_click;
        for (var k = 0; k < popups_click_count; k++) {
            popup_click = popupsClick[k];
            if (popup_click.id === id) {
                popup_click.popup.setContent('<p><div id="' + country_id + 'POP" class="country-popup popup" data-element="' + country_id + '" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; margin-left: -50px; margin-top: -25px; z-index: 400; top: 20px" href="callback:' + country_id + '"><div style="color: white; background-color: black; width: 140px; margin-left: 37px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 20px">' + country.name + '</span></div><div style="width: 143px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 22px"> LF: <span id="' + country_id + 'LF">' + Math.round(country.lf) + '</span>% <span style="padding-left: 14px">AP: <span id="' + country_id + 'AP">' + Math.round(country.ap) + '</span>MW</span></div></div>')
            }
        }
    } else {
        $('#' + id + 'LF').html(Math.round(lf));
        $('#' + id + 'AP').html(Math.round(ap));
    }
}

function updateRegion(id, lf, ap, online, offline, nocomm) {

    var node_regions = tree_nodes.findNodeInLevel(3, id);
    var region = node_regions.value;
    var region_id = region.id;

    region.ap = ap;
    region.lf = lf;
    region.online = online;
    region.offline = offline;
    region.nocomm = nocomm;

    var ctx_arr_count = ctxArray.length;
    var ctx_arr_curr, ctx_curr;
    for (var i = 0; i < ctx_arr_count; i++) {
        ctx_arr_curr = ctxArray[i];

        if (ctx_arr_curr.id === id) {
            var distance;
            if (region.acronym.length === 1) {
                distance = 17;
            } else {
                distance = 11;
            }

            var total = region.online + region.offline + region.nocomm;
            var greenSize = region.online * 2 / total;
            var redSize = region.offline * 2 / total;

            ctx_curr = ctx_arr_curr.ctx;

            setUpParkForStatus(region, ctx_curr, greenSize, redSize);

            ctx_curr.font = "16pt Helvetica-Th";
            ctx_curr.fillStyle = "black";
            ctx_curr.textAlign = "middle";
            ctx_curr.fillText(region.acronym, distance, 35);

            break;
        }
    }

    if (map.getZoom() >= setParkRegionCircleMin && map.getZoom() <= setParkRegionCircleMax) {
        var ctx_circle_arr_count = ctxCircleLabels.length;
        var ctx_circle_arr_lbl, ctx_circle_lbl;
        for (var z = 0; z < ctx_circle_arr_count; z++) {
            ctx_circle_arr_lbl = ctxCircleLabels[z];
            if (ctx_circle_arr_lbl.id.substring(0, 3) === id) {
                ctx_circle_lbl = ctx_circle_arr_lbl.ctx;
                ctx_circle_lbl.clearRect(0, 0, 160, 30);

                ctx_circle_lbl.font = '12pt Helvetica-Cn';
                ctx_circle_lbl.fillStyle = '#ffffff';
                ctx_circle_lbl.fillText(region.name, 20, 14);

                ctx_circle_lbl.closePath();

                ctx_circle_lbl.font = '9pt Helvetica-LtCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('LF: ', 20, 29);

                ctx_circle_lbl.font = '10pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText(Math.round(region.lf), 40, 29);

                ctx_circle_lbl.font = '9pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('%', 60, 29);

                ctx_circle_lbl.font = '9pt Helvetica-LtCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('AP: ', 80, 29);

                ctx_circle_lbl.font = '10pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText(Math.round(region.ap), 100, 29);

                ctx_circle_lbl.font = '9pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('mW', 125, 29);

                ctx_circle_lbl.closePath();

                break;
            }
        }
    }

    if ($('.region-canvas-popup').length === 0) {
        var popup_count = popupsClick.length;
        var pop_click;
        for (var k = 0; k < popup_count; k++) {
            pop_click = popupsClick[k];
            if (pop_click.id === id) {
                pop_click.popup.setContent('<p><div id="' + region_id + 'POP" class="region-popup popup" data-element="' + region_id + '" style="display: none; box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative;  margin-left: -50px; margin-top: -25px; z-index: 400; top: 20px" href="callback:' + region_id + '"><div style="color: white; background-color: black; width: 140px; margin-left: 37px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 20px">' + region.name + '</span></div><div style="width: 143px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 22px"> LF: <span id="' + region_id + 'LF">' + Math.round(region.lf) + '</span>% <span style="padding-left: 14px">AP: <span id="' + region_id + 'AP">' + Math.round(region.ap) + '</span>MW</span></div></div>');
                break;
            }
        }
    } else {
        $('#' + id + 'LF').html(Math.round(lf));
        $('#' + id + 'AP').html(Math.round(ap));
    }
}

function callNativeApp(ele) {
    try {
        if (ele.dataset.element.indexOf('-') > 0) {
            ele.dataset.element = ele.dataset.element.slice(0, ele.dataset.element.indexOf('-'));
        }

        var notParent = ele.dataset.parent;
        if (ele.dataset.type !== 'asset') {
            notParent = undefined;
        }

        var message = {
            "parent": notParent,
            "id": ele.dataset.element
        }

        webkit.messageHandlers.callbackHandler.postMessage(message);
    } catch (err) {
        console.log('IOS error');
    }

    try {
        if (ele.dataset.element.indexOf('-') > 0) {
            ele.dataset.element = ele.dataset.element.slice(0, ele.dataset.element.indexOf('-'));
        }
        Android.showInformation(ele.dataset.parent, ele.dataset.element);
    } catch (err) {
        console.log('ANDROID error')
    }
}

function updateAsset(assetID, parkID, lf, ap, status) {

    var color;
    switch (status) {
        case 100: // run
            color = '#39B54A';
            break;
        case 200: // ready
            color = '#0071BC';
            break;
        case 300: // stop
            color = '#C1272D';
            break;
        case 400: // maint
            color = '#FCEE21';
            break;
        case 500: // restricted
            color = '#F7931E';
            break;
        case 600:
        case 301: // repair
            color = '#A67C52';
            break;
        default: // nocomm
            color = '#D927BF';
            break;
    }

    var node_park = tree_nodes.findNodeInLevel(4, parkID);
    var node_park_assets = node_park.children;

    var park = node_park.value;

    var assets_count = node_park_assets.length;
    var asset;
    for (var i = 0 ; i < assets_count ; i++) {
        asset = node_park_assets[i].value;

        if (asset.id === assetID) {
            asset.ap = ap;
            asset.lf = lf;
            asset.status = status;

            //if (map.getZoom() > setAssetsZoom) {
            //assets.clearLayers();
            //setAssets();

            var ctx_arr_count = ctxArray.length;
            var ctx_arr_curr, ctx_curr;
            for (var ctx_i = 0; ctx_i < ctx_arr_count; ctx_i++) {
                ctx_arr_curr = ctxArray[ctx_i];

                if (ctx_arr_curr.assetID === assetID && ctx_arr_curr.parkID === parkID) {
                    ctx_curr = ctx_arr_curr.ctx;
                    ctx_curr.clearRect(-50, -50, 50, 50);

                    drawArc(ctx_curr, 0, 2, color);

                    if (asset.energyType === 'WTG') {
                        assetWindConstructor(ctx_curr);
                    } else if (asset.energyType === 'INV') {
                        assetSolarConstructor(ctx_curr);
                    } else {
                        assetHydroConstructor(ctx_curr);
                    }
                }
            }

            if ($('.asset-popup').length === 0) {
                var popup_click_count = popupsClick.length;
                var popup_click;
                for (var k = 0; k < popup_click_count; k++) {
                    popup_click = popupsClick[k];
                    if (popup_click.assetID === assetID && popup_click.parkID === parkID) {
                        popup_click.popup.setContent('<p><div id="' + assetID + 'POP" class="asset-marker popup" data-element="' + assetID + '" data-parent="' + parkID + '" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 113px; position: absolute; top: 14px; left: 20px;  margin-left: -35px;" href="callback: park:' + parkID + ', asset:' + assetID + '"><div id=' + parkID + assetID + 'BAR style="color: white; background-color:' + color + '; width: 142px; margin-top: -16px; padding-top: 1px; margin-left: 6px;"><span style="padding-left: 16px">' + assetID + '</span></div><div style="width: 122px; background-color: white; font-size: 10.2px; margin-left: 6px; padding-left: 20px;">LF: <span id="' + parkID + assetID + 'LF">' + Math.round(asset.lf) + '</span>% <span style="padding-left: 14px">AP: <span id="' + parkID + assetID + 'AP">' + Math.round(asset.ap) + '</span>mw </span></span></div></div>');
                        break;
                    }
                }
            } else {
                $('#' + parkID + assetID + 'LF').html(Math.round(lf));
                $('#' + parkID + assetID + 'AP').html(Math.round(ap));
                $('#' + parkID + assetID + 'BAR').css('background-color', color);
            }


            if (park_selected !== null && map.getZoom() > setAssetsZoom) {
                var park_id = park_selected.id;
                $('.park-label-' + park_id).css('display', 'inline-block');
                $('.test-label-park-' + park_id).css('display', 'inline-block');
                $('.parks-icons-' + park_id).css('display', 'none');

                $('.asset-icons').css('display', 'none');
                $('.asset-icons-' + park_id).css('display', 'inline-block');
                $('.asset-icons-' + park_id).css('zIndex', '1000')
                $('.park-popup').css('display', 'none');
            } else {
                $('.asset-icons').css('display', 'none');
            }

            break;
        }
    }

    $('.asset-icons ').css('zIndex', 10);
};

function updatePark(id, lf, ap, online, offline, nocomm, weather) {

    var node_park = tree_nodes.findNodeInLevel(4, id);
    var park = node_park.value;
    var park_id = park.id;

    park.ap = ap;
    park.lf = lf;
    park.online = online;
    park.offline = offline;
    park.nocomm = nocomm;
    park.weather = weather;

    var ctx_arr_count = ctxArrayParks.length;
    var ctx_arr_curr, ctx_curr;
    for (var i = 0; i < ctx_arr_count; i++) {
        ctx_arr_curr = ctxArrayParks[i];
        if (ctx_arr_curr.id === id) {
            ctx_curr = ctx_arr_curr.ctx;

            if (typeof park.online == 'undefined')
                park.online = 0;

            if (typeof park.offline == 'undefined')
                park.offline = 0;

            if (typeof park.noComm == 'undefined')
                park.noComm = 0;

            var total = park.online + park.offline + park.noComm;
            var greenSize = 0;
            var redSize = 0;

            if (total != 'undefined' && total > 0) {
                greenSize = park.online * 2 / total;
                redSize = park.offline * 2 / total;
            }

            setUpParkForStatus(park, ctx_curr, greenSize, redSize);

            switch (park.energyType) {
                case 'WIND':
                    parkWindConstructor(ctx_curr);
                    break;
                case 'SOLAR':
                    parkSolarConstructor(ctx_curr);
                    break;
                case 'HYDRO':
                    parkHydroConstructor(ctx_curr);
                    break;
            }

        }
    }
    //eolics = undefined;
    ctxArrayParks = [];
    /*eolics.clearLayers();

    setParks();*/

    if (map.getZoom() > setAssetsZoom) {
        var ctx_circle_arr_count = ctxCircleLabels.length;
        var ctx_circle_arr_lbl, ctx_circle_lbl;
        for (var z = 0; z < ctx_circle_arr_count; z++) {
            ctx_circle_arr_lbl = ctxCircleLabels[z];
            if (ctx_circle_arr_lbl.id.substring(0, 3) === id) {
                ctx_circle_lbl = ctxCircleLabels[z].ctx;
                ctx_circle_lbl.clearRect(0, 0, 160, 30);

                ctx_circle_lbl.font = '12pt Helvetica-Cn';
                ctx_circle_lbl.fillStyle = '#ffffff';
                ctx_circle_lbl.fillText(park.name, 20, 14);

                ctx_circle_lbl.closePath();

                ctx_circle_lbl.font = '9pt Helvetica-LtCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('LF: ', 20, 29);

                ctx_circle_lbl.font = '10pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText(Math.round(park.lf), 40, 29);

                ctx_circle_lbl.font = '9pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('%', 60, 29);

                ctx_circle_lbl.font = '9pt Helvetica-LtCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('AP: ', 80, 29);

                ctx_circle_lbl.font = '10pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText(Math.round(park.ap), 100, 29);

                ctx_circle_lbl.font = '9pt Helvetica-BdCn';
                ctx_circle_lbl.fillStyle = '#000000';
                ctx_circle_lbl.fillText('mW', 125, 29);

                ctx_circle_lbl.closePath();

                ctx_circle_lbl.beginPath();
                ctx_circle_lbl.rect(150, 0, 32, 32);
                ctx_circle_lbl.fillStyle = '#FC7E3F';
                ctx_circle_lbl.fill();
                ctx_circle_lbl.closePath();

                ctx_circle_lbl.beginPath();
                ctx_circle_lbl.font = '18pt Meteo';
                ctx_circle_lbl.fillStyle = '#ffffff';
                ctx_circle_lbl.fillText(getWeather(park.weather), 153, 27);
                ctx_circle_lbl.closePath();

                break;
            }
        }
    }



    if ($('.park-canvas-popup').length === 0) {
        var popup_count = popupsClick.length;
        var pop_click;
        for (var k = 0; k < popup_count; k++) {
            pop_click = popupsClick[k];
            if (pop_click.id === id) {
                pop_click.popup.setContent('<div id="' + park_id + '" class="park-popup ' + park_id + 'POP popup" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; margin-left: -55px; margin-top: -20px; z-index: 400; top: 5px" href="callback:' + park_id + '"><div style="color: white; background-color: black; width: 141px; margin-left: 40px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 14px">' + park.name + '</span></div><div style="width: 146px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 19px"> LF: <span id="' + park_id + 'LF">' + Math.round(park.lf) + '</span>% <span style="padding-left: 14px">AP: <span id="' + park_id + 'AP">' + Math.round(park.ap) + '</span>mw</span></div></div>');
                break;
            }
        }
    } else {
        $('#' + id + 'LF').html(Math.round(lf));
        $('#' + id + 'AP').html(Math.round(ap));
    }

    //if (park_selected !== null) {
    //    if ((park_selected.id === id && map.getZoom() > setAssetsZoom) || park_selected.id !== id) {
    //        $('.parks-icons-' + park_selected.id).css('display', 'none');
    //    }
    //}
    //    }
    //}
}

function drawArc(ctx, start, size, color) {
    var radius = 23

    if (size <= 0) {
        return;
    }

    var end = start + size;

    // adjust positions so the drawing starts at the top
    start = -0.5 + start;
    end = end > 2 ? 1.5 : end - 0.5;

    // add margin
    if ((color === '#D927BF' && start !== -0.5) ||
        (end === 1.5 && start !== -0.5) ||
        (end !== 1.5)) {
        end -= 0.03;
    }

    start = start > end ? end : start;

    //ctx.translate(50 / 2, 50 / 2);
    ctx.beginPath();
    ctx.arc(50 / 2, 50 / 2, 21, 0, 2 * Math.PI);
    ctx.fillStyle = "white";
    ctx.fill();
    ctx.closePath();
    ctx.beginPath();

    ctx.arc(50 / 2, 50 / 2, radius, start * Math.PI, end * Math.PI);
    ctx.lineWidth = 4;
    ctx.strokeStyle = color;
    //ctx.fillStyle = "white";
    //ctx.fill();
    ctx.stroke();
    ctx.closePath();

    //ctx.translate(-50/2,-50/2);
}

function assetWindConstructor(ctx) {
    ctx.font = "25pt RMS";
    ctx.fillStyle = "black";
    ctx.textAlign = "middle";
    ctx.fillText("E", 9, 40);
};

function assetSolarConstructor(ctx) {
    ctx.font = "25pt RMS";
    ctx.fillStyle = "black";
    ctx.textAlign = "middle";
    ctx.fillText("F", 9, 40);
};

function assetHydroConstructor(ctx) {
    ctx.font = "25pt RMS";
    ctx.fillStyle = "black";
    ctx.textAlign = "middle";
    ctx.fillText("G", 9, 40);
};

function parkWindConstructor(ctx) {
    ctx.font = "25pt RMS";
    ctx.fillStyle = "black";
    ctx.textAlign = "middle";
    ctx.fillText("A", 9, 40);
};

function parkSolarConstructor(ctx) {
    ctx.font = "25pt RMS";
    ctx.fillStyle = "black";
    ctx.textAlign = "middle";
    ctx.fillText("B", 9, 40);
};

function parkHydroConstructor(ctx) {
    ctx.font = "25pt RMS";
    ctx.fillStyle = "black";
    ctx.textAlign = "middle";
    ctx.fillText("C", 9, 40);
};

function setUpParkForStatus(item, ctx, green, red) {
    var greenSize = green > 0 ? green : 0;
    var redSize = red > 0 ? red : 0;

    //if (firstDraw) {
    item.statusIndicator = {
        green: greenSize,
        red: redSize
    };
    ctx.clearRect(0, 0, 50, 50);

    ctx.beginPath();
    ctx.arc(50 / 2, 50 / 2, 24, 0, 2 * Math.PI);
    ctx.fillStyle = "white";
    ctx.fill();
    ctx.closePath();

    drawArc(ctx, 0, item.statusIndicator.green, '#39B54A');
    drawArc(ctx, item.statusIndicator.green, item.statusIndicator.red, '#C1272D');
    drawArc(ctx, item.statusIndicator.green + item.statusIndicator.red, 2 - (item.statusIndicator.green + item.statusIndicator.red), '#D927BF');
    //}
};

function getWeather(weather) {
    var weatherClass;
    switch (weather) {
        case 'Wind and Rain':
            weatherClass = 'A';
            break;
        case 'Wind and Clouds':
            weatherClass = 'B';
            break;
        case 'Snow Flakes':
            weatherClass = 'C';
            break;
        case 'Snow':
            weatherClass = 'D';
            break;
        case 'Clouds':
            weatherClass = 'E';
            break;

        case 'Partly Cloudy':
            weatherClass = 'F';
            break;

        case 'Mostly Cloudy':
            weatherClass = 'G';
            break;

        case 'Thunderstorm':
            weatherClass = 'H';
            break;

        case 'Drizzle':
            weatherClass = 'I';
            break;

        case 'Rain':
            weatherClass = 'J';
            break;

        case 'Sun':
            weatherClass = 'K';
            break;

        case 'Wind':
            weatherClass = 'L';
            break;

        case 'Fog':
            weatherClass = 'M';
            break;
    }
    return weatherClass;
};

function popupsManage(e) {
    if ($('#' + this.options.id + 'POP')[0]) {
        if ($('#' + this.options.id + 'POP')[0].style.display) {
            if ($('#' + this.options.id + 'POP')[0].style.display !== 'none') {
                $('#' + this.options.id + 'POP')[0].style.display = 'none';
            } else {
                $('#' + this.options.id + 'POP').css('display', 'inline-block');
            }
        } else {
            $('#' + this.options.id + 'POP').css('display', 'inline-block');
        }

    }

    if ($('.' + this.options.id + 'POP')[0]) {
        if ($('.' + this.options.id + 'POP')[0].style.visibility !== 'hidden') {
            $('.' + this.options.id + 'POP')[0].style.visibility = 'hidden';
        } else {
            $('.' + this.options.id + 'POP').css('visibility', 'visible');
        }
    }
}

$('#leafletMap').on('click', '.popup', function () {
    callNativeApp(this)
});

$('#leafletMap').on('click', '.test-label', function () {
    callNativeApp(this);
});

// FOR MOBILE 
function createOwner(id, name, lat, lon, radius, lf, ap) {
    var loc = new Location(lat, lon);
    owner = new Owner(id, name, loc, radius, lf, ap);

    tree_nodes.addChild(tree_root, new Node(owner));
}

function createCountry(id, name, acronym, lat, lon, radius, lf, ap, online, offline, nocomm, weather, parentId) {

    var node_owner = tree_nodes.getNodesInLevel(1)[0];

    var loc = new Location(lat, lon);
    var country = new Country(id, name, acronym, loc, radius, lf, ap, online, offline, nocomm, weather);

    tree_nodes.addChild(node_owner, new Node(country));
}

function createRegion(id, name, acronym, lat, lon, radius, lf, ap, online, offline, nocomm, weather, parentId) {

    var node_country = tree_nodes.findNodeInLevel(2, parentId);

    var loc = new Location(lat, lon);
    var region = new Region(id, name, acronym, loc, radius, lf, ap, online, offline, nocomm, weather);

    tree_nodes.addChild(node_country, new Node(region));
}

var arrayParks = [];
function createPark(id, name, energyType, lat, lon, radius, weather, online, offline, nocomm, ap, lf, parentId) {

    var node_region = tree_nodes.findNodeInLevel(3, parentId);

    var assets = [];
    var loc = new Location(lat, lon);
    var park = new Park(id, name, energyType, assets, loc, radius, lf, ap, online, offline, nocomm, weather);

    tree_nodes.addChild(node_region, new Node(park));

    isReady = 1;
}

function createAsset(id, name, energyType, lat, lon, status, ap, lf, parentId) {

    var node_park = tree_nodes.findNodeInLevel(4, parentId);

    var loc = new Location(lat, lon);
    var park_asset = new Asset(id, name, energyType, loc, status, lf, ap);

    node_park.value.assets.push(id);

    tree_nodes.addChild(node_park, new Node(park_asset));
}

var greenMarker;
function createMarker(lat, lon) {
    greenMarker = L.marker([lat, lon], {
        //icon: greenIcon,
        draggable: 'true'
    }).bindPopup('<div style="background-color: white">' + lat + ', ' + lon + '</div>').addTo(map);

    /*greenMarker.on('dragend', function (e) {
        greenMarker._popup.setContent('<div style="background-color: white; top: -15px; position:relative;">' + greenMarker.getLatLng().lat + ', ' + greenMarker.getLatLng().lng + '</div>');
    })*/
}

function deleteMarker() {
    map.removeLayer(greenMarker);
}